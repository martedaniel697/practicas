﻿using System;

namespace program4
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Crear un programa que pida al usuario un número entero. Si es múltiplo de 10,
          se lo avisará al usuario y pedirá un segundo número, para decir a continuación
          si este segundo número también es múltiplo de 10.*/
            int numero = 10;
            Console.WriteLine("Introduce el primer numero");
            int num1 = Convert.ToInt32(Console.ReadLine());
            if (num1 % numero == 0)
            {
                Console.WriteLine("El numero " + num1 + " es multiplo de 10");
            }
            else
            {
                Console.WriteLine("El numero " + num1 + " no es multiplo de 10 ");
            }
            Console.WriteLine("Introduce el segundo numero");
            int num2 = Convert.ToInt32(Console.ReadLine());
            if (num2 % numero == 0)
            {
                Console.WriteLine("El numero " + num2 + " es multiplo de 10");
            }
            else
            { Console.WriteLine("El numero " + num2 + " no es multiplo de 10"); }
            Console.ReadKey();
        }
    }
}
