﻿using System;

namespace program8
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Crear un programa que pida al usuario dos números enteros y diga &quot;Uno de los
              números es positivo&quot;, &quot;Los dos números son positivos&quot; o bien &quot;Ninguno de los
              números es positivo&quot;, según corresponda.*/
            Console.WriteLine("Introduce un numero");
            int num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce otro numero");
            int num2 = Convert.ToInt32(Console.ReadLine());
            if (num1 > 0 && num2 > 0)
            {
                Console.WriteLine("los 2 numeros son positivos");
            }
            if (num1>0 && num2 < 0)
            {
                Console.WriteLine(" El primer numero es positivo y el segundo es negativo");
            }
            if (num1 < 0 && num2 > 0)
            {
                Console.WriteLine("El primer numero es negativo y el segundo positivo");
            }

            if (num1 < 0 && num2 < 0)
            {
                Console.WriteLine("los 2 numero son negativos");
            }
        }
    }
}
