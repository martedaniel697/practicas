﻿using System;
using System.Reflection.Metadata.Ecma335;

namespace program5
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Crear un programa que multiplique dos números enteros de la siguiente forma:
            pedirá al usuario un primer número entero. Si el número que se que teclee es 0,
            escribirá en pantalla &quot;El producto de 0 por cualquier número es 0&quot;. Si se ha
            tecleado un número distinto de cero, se pedirá al usuario un segundo número y
            se mostrará el producto de ambos.*/
            Console.Write("Introduce el primer numero:  ");
            int num1 = Convert.ToInt32(Console.ReadLine());
            if (num1 > 0) 
            {
                Console.WriteLine("introduce el segundo numero");
                int num2 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("El producto es: " + num1 * num2);
            }
            else
            {
                Console.WriteLine("El producto de 0 por cualquier numero es 0");
            }
            Console.ReadKey();
            
            


        }
    }
}
