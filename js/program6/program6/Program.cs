﻿using System;

namespace program6
{
    class Program
    {
        static void Main(string[] args)
        {/*Crear un programa que pida al usuario dos números enteros. Si el segundo no
           es cero, mostrará el resultado de dividir entre el primero y el segundo. Por el
           contrario, si el segundo número es cero, escribirá &quot;Error: No se puede dividir
           entre cero&quot;.*/
            Console.Write("Introdusca el primer numero: ");
            int num1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introdusca el segundo numero: ");
            int num2 = Convert.ToInt32(Console.ReadLine());
            if (num2 == 0)
            {
                Console.WriteLine("ERROR: no se puede dividir entre 0");
            }
            else
            {
                Console.Write("La divicion de los numero es: " + num1 / num2);
            }

            Console.ReadKey();
        }
    }
}
