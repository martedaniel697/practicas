﻿using System;

namespace program7
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Crear un programa que pida al usuario un número enteros y diga si es múltiplo
               de 4 o de 5.*/
            int tabla1 = 4;
            int tabla2 = 5;
            Console.WriteLine("Introduce el numero");
            int numero = Convert.ToInt32(Console.ReadLine());
            if (numero % tabla1 == 0)
            {
                Console.WriteLine("El numero " + numero + " es multiplo de 4");
            }
            if (numero % tabla2 == 0)
            {
                Console.WriteLine("El numero " + numero + " es multiplo de 5");
            }
            Console.ReadKey();

        }
    }
}
