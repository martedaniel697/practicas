﻿using System;

namespace program4 
{
    class Program
    {
        static void Main(string[] args)
        {/*Crear un programa que pida al usuario un número entero. Si es múltiplo de 10,
          se lo avisará al usuario y pedirá un segundo número, para decir a continuación
          si este segundo número también es múltiplo de 10.*/

            Console.WriteLine("Introduce el primer numero");
            int num1 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Introduce el segundo numero");
            int num2 = Convert.ToInt32(Console.ReadLine());
            if (num1 % num2 == 0)
            {
                Console.WriteLine("El primer numero es multiplo del segundo");
            }
            else
            { Console.WriteLine("El segundo es multiplo del primero"); }
            Console.ReadKey();
        }
    }
}
